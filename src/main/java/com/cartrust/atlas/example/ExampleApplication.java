package com.cartrust.atlas.example;

import com.cartrust.atlas.ssikit.EnableAtlas;
import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.info.Info;
import io.swagger.v3.oas.annotations.servers.Server;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@EnableAtlas
@EnableScheduling
@OpenAPIDefinition(
        info = @Info(
                title = "Example SSI service made by carTRUST by Kroschke",
                version = "0.1",
                description = "API to show SSI usage"
        ),
        servers = {
                @Server(url = "/", description = "Default Server URL")
        }
)
@SpringBootApplication
public class ExampleApplication {
    public static void main(String[] args) {
        SpringApplication.run(ExampleApplication.class, args);
    }
}
