package com.cartrust.atlas.example;

import com.cartrust.atlas.ssikit.AtlasCommunicator;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.eclipse.paho.client.mqttv3.IMqttClient;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.net.URI;
import java.nio.charset.StandardCharsets;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

@Slf4j
@RestController
@RequestMapping(path = "example/sender")
public class ExampleSenderController {
    private final AtlasCommunicator communicator;
//    private final IMqttClient mqttClient;

    @SneakyThrows
    public ExampleSenderController(
            AtlasCommunicator communicator
//            , IMqttClient mqttClient
    ) {
        this.communicator = communicator;
//        this.mqttClient = mqttClient;
//
//        mqttClient.subscribe("TEST", (topic, msg) -> {
//            byte[] payload = msg.getPayload();
//            log.info("Got: {}", new String(payload));
//        });
    }

    @PostMapping(value = "send", consumes = MediaType.TEXT_PLAIN_VALUE, produces = MediaType.TEXT_PLAIN_VALUE)
    public ResponseEntity<String> sendAuthorizedRequest(@RequestBody String input) {
        URI uri = URI.create("http://localhost:9180/example/receiver/receive");
        HttpEntity<String> httpEntity = communicator.createHttpEntity(input, MediaType.TEXT_PLAIN, true);
        return communicator.exchange(HttpMethod.POST, httpEntity, uri, String.class);
    }
//
//    @PostMapping(value = "mqtt", consumes = MediaType.TEXT_PLAIN_VALUE, produces = MediaType.TEXT_PLAIN_VALUE)
//    public ResponseEntity<String> mqtt(@RequestBody String input) {
//
//
//        if (mqttClient.isConnected()) {
//            MqttMessage msg = new MqttMessage(input.getBytes(StandardCharsets.UTF_8));
//            msg.setQos(0);
//            msg.setRetained(true);
//            try {
//                mqttClient.publish("TEST",msg);
//                return ResponseEntity.ofNullable("OK");
//            } catch (MqttException e) {
//                return ResponseEntity.ofNullable("ENOK");
//            }
//        }
//
//        return ResponseEntity.ofNullable("NOK");
//    }
}
