package com.cartrust.atlas.example;

import lombok.extern.slf4j.Slf4j;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@Slf4j
@RestController
@RequestMapping(path = "example/receiver")
public class ExampleReceiverController {

    @PostMapping(value = "receive", consumes = MediaType.TEXT_PLAIN_VALUE, produces = MediaType.TEXT_PLAIN_VALUE)
    public ResponseEntity<String> receiveAuthorizedRequest(@RequestBody String data, @RequestHeader(value = "Authorization") String auth) {
        log.info("Authorization: {}", auth);
        return ResponseEntity.ofNullable("Authorized " + data);
    }
}
