package com.cartrust.atlas.example.distance;

import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class DistanceDto {
    private String destinationAddress;
    private String originAddress;
    private long meters;
    private String readable;
}
