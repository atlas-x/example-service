package com.cartrust.atlas.example.distance;

import id.walt.auditor.VerificationPolicy;
import id.walt.auditor.VerificationPolicyResult;
import id.walt.credentials.w3c.VerifiableCredential;
import id.walt.credentials.w3c.VerifiablePresentation;
import org.jetbrains.annotations.NotNull;

import java.util.Objects;

public class DistanceVerificationPolicy extends VerificationPolicy {
    @NotNull
    @Override
    public String getDescription() {
        return "Policy to check distance service access";
    }

    @NotNull
    @Override
    protected VerificationPolicyResult doVerify(@NotNull VerifiableCredential vc) {
        if (vc instanceof VerifiablePresentation vp) {
            boolean presentationHasSelfDescription = Objects.requireNonNull(vp.getVerifiableCredential())
                    .stream()
                    .anyMatch(this::isSelfDescriptionVC);
            return presentationHasSelfDescription
                    ? VerificationPolicyResult.Companion.success()
                    : VerificationPolicyResult.Companion.failure(new Exception("DistancePolicy requires presented self description"));
        }
        if (!isSelfDescriptionVC(vc) || isAtlasSubsystem(vc)) {
            return VerificationPolicyResult.Companion.success();
        }
        return VerificationPolicyResult.Companion.failure();
    }

    private boolean isSelfDescriptionVC(VerifiableCredential vc) {
        return vc.getType().contains("SelfDescription") || vc.getType().contains("GaiaxCredential");
    }

    private boolean isAtlasSubsystem(VerifiableCredential vc) {
        String subjectId = vc.getSubjectId();
        return Objects.requireNonNull(subjectId).matches("did:web:[a-z]+.atlas.cartrust.com:.*");
    }
}
