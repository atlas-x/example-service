package com.cartrust.atlas.example.distance;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class DistanceProperties {
    private String googleApiKey;
}
