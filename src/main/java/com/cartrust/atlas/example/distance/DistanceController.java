package com.cartrust.atlas.example.distance;

import com.google.maps.DistanceMatrixApi;
import com.google.maps.DistanceMatrixApiRequest;
import com.google.maps.GeoApiContext;
import com.google.maps.model.Distance;
import com.google.maps.model.DistanceMatrix;
import com.google.maps.model.TravelMode;
import com.google.maps.model.Unit;
import lombok.SneakyThrows;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("distance")
public class DistanceController {

    private final DistanceProperties properties;

    public DistanceController(DistanceProperties properties) {
        this.properties = properties;
    }

    @SneakyThrows
    @GetMapping
    public DistanceDto get(
            @RequestParam String origin,
            @RequestParam String destination,
            @RequestHeader(value = "Authorization") String auth
    ) {
        GeoApiContext context = new GeoApiContext.Builder()
                .apiKey(properties.getGoogleApiKey())
                .build();
        DistanceMatrixApiRequest matrixApiRequest = DistanceMatrixApi.newRequest(context);
        matrixApiRequest.units(Unit.METRIC);
        matrixApiRequest.mode(TravelMode.DRIVING);
        matrixApiRequest.origins(origin);
        matrixApiRequest.destinations(destination);
        DistanceMatrix matrix = matrixApiRequest.await();

        DistanceDto.DistanceDtoBuilder builder = DistanceDto.builder();
        if (matrix.rows.length > 0 && matrix.rows[0].elements.length > 0) {
            Distance distance = matrix.rows[0].elements[0].distance;
            builder
                    .meters(distance.inMeters)
                    .readable(distance.humanReadable);
        }

        builder
                .destinationAddress(matrix.destinationAddresses[0])
                .originAddress(matrix.originAddresses[0]);
        return builder.build();
    }
}
