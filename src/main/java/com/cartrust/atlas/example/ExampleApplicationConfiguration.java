package com.cartrust.atlas.example;

import com.cartrust.atlas.example.distance.DistanceProperties;
//import org.eclipse.paho.client.mqttv3.IMqttClient;
//import org.eclipse.paho.client.mqttv3.MqttClient;
//import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
//import org.eclipse.paho.client.mqttv3.MqttException;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.UUID;

@Configuration
public class ExampleApplicationConfiguration {
    @Bean
    @ConfigurationProperties(prefix = "app.distance")
    public DistanceProperties distanceProperties() {
        return new DistanceProperties();
    }
//
//    @Bean
//    IMqttClient mqttClient() {
//        try {
//            String publisherId = UUID.randomUUID().toString();
////            IMqttClient publisher = new MqttClient("tcp://iot.eclipse.org:1883",publisherId);
//            IMqttClient publisher = new MqttClient("tcp://localhost:1883",publisherId);
//            MqttConnectOptions options = new MqttConnectOptions();
//            options.setAutomaticReconnect(true);
//            options.setCleanSession(true);
//            options.setConnectionTimeout(10);
//            publisher.connect(options);
//
//            return publisher;
//        } catch (MqttException e) {
//            throw new RuntimeException(e);
//        }
//    }


}
