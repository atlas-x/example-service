package com.cartrust.atlas.example;

import com.cartrust.atlas.example.distance.DistanceVerificationPolicy;
import com.cartrust.atlas.ssikit.AtlasCommunicator;
import com.cartrust.atlas.ssikit.catalogue.AtlasCatalogue;
import com.cartrust.atlas.ssikit.catalogue.AtlasCatalogueConfiguration;
import com.cartrust.atlas.ssikit.catalogue.CataloguePublisher;
import com.cartrust.atlas.ssikit.config.AtlasConfigProperties;
import com.cartrust.atlas.ssikit.config.AtlasInitializer;
import com.cartrust.atlas.ssikit.config.AtlasJwtConfigBuilder;
import com.cartrust.atlas.ssikit.config.OpenApiAccessor;
import com.cartrust.atlas.ssikit.gx.AtlasServiceOfferingManager;
import com.cartrust.atlas.ssikit.policies.AtlasSignaturePolicy;
import com.cartrust.atlas.ssikit.policies.PresentedBySubjectVerificationPolicy;
import com.cartrust.atlas.ssikit.waltid.AtlasVcTemplateService;
import com.cartrust.atlas.ssikit.waltid.FSHKVStore;
import com.cartrust.atlas.ssikit.waltid.S3HKVStoreProperties;
import com.cartrust.atlas.ssikit.waltid.S3HkvStore;
import com.fasterxml.jackson.databind.ObjectMapper;
import id.walt.services.hkvstore.HKVStoreService;
import id.walt.signatory.Signatory;
import org.springdoc.core.models.GroupedOpenApi;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.List;

@Configuration
public class ExampleAtlasConfiguration {
    @Bean
    public AtlasCatalogueConfiguration atlasCatalogueConfiguration() {
        return AtlasCatalogueConfiguration.builder()
                .peer("https://authority.atlas.cartrust.com/gx/catalogue")
                .build();
    }

    @Bean
    @ConfigurationProperties("atlas.hkv")
    public S3HKVStoreProperties s3HKVStoreProperties() {
        return new S3HKVStoreProperties();
    }
    @Bean
    public HKVStoreService hkvStore(S3HKVStoreProperties properties) {
        return new S3HkvStore(properties, null);
    }

//    @Bean
//    public HKVStoreService hkvStore() {
//        return new FSHKVStore("hkv-example");
//    }

    @Bean(name = "exampleGroupedOpenApi")
    public GroupedOpenApi exampleGroupedOpenApi() {
        return GroupedOpenApi.builder()
                .group("example")
                .packagesToScan("com.cartrust.atlas.example")
                .pathsToMatch("/example/**")
                .build();
    }

    @Bean
    public GroupedOpenApi distanceGroupedOpenApi() {
        return GroupedOpenApi.builder()
                .group("ServiceOfferingDistance")
                .packagesToScan("com.cartrust.atlas.example.distance")
                .pathsToMatch("/distance/**")
                .build();
    }

    @Bean
    public AtlasServiceOfferingManager serviceOfferingManager(
            AtlasConfigProperties configuration,
            AtlasInitializer atlasInitializer,
            Signatory signatoryService,
            AtlasVcTemplateService templateService,
            OpenApiAccessor openApiAccessor,
            ObjectMapper mapper
    ) {
        return new AtlasServiceOfferingManager(
                configuration,
                atlasInitializer,
                signatoryService,
                templateService,
                openApiAccessor,
                mapper
        );
    }

    @Bean
    public AtlasJwtConfigBuilder atlasJwtConfigBuilder(
            AtlasSignaturePolicy signatureValidator
    ) {
        PresentedBySubjectVerificationPolicy presentedPolicy = new PresentedBySubjectVerificationPolicy();
        DistanceVerificationPolicy distancePolicy = new DistanceVerificationPolicy();
        return configurer -> {
            configurer.configure("/example/receiver.*").post(List.of(signatureValidator, presentedPolicy));
            configurer.configure("/example/distance.*").all(List.of(signatureValidator, presentedPolicy, distancePolicy));
        };
    }

    @Bean
    public CataloguePublisher cataloguePublisher(
            AtlasCatalogue catalogue,
            AtlasConfigProperties configProperties,
            AtlasCommunicator communicator,
            ObjectMapper objectMapper
    ) {
        return new CataloguePublisher(
                catalogue,
                configProperties,
                communicator,
                objectMapper
        );
    }
}
